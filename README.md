# Home network infrastructure

This is divided in several layers for which different technologies or methods
are responsible for deployment, provisioning and configuration.

## Network

### Deployment

This is done manually because this is still a physical process.


### Provisioning and configuration

Handled by Ubiquiti Unify controller.
Need to check how to backup and reload configuration in the event I need to
start from scratch.


## Core network services

Core services are the basic essentials needed to run the network:
- DHCP
- DNS
- PXE network boot for provisionning other hosts

## Services

## Clients 

Mainly laptops and smartphones. They get their network configuration from DHCP.
Laptops are configured to be backed up regularly.

## Requirements

```
sudo apt install sshpass rsync
```

### SSH key

Generate a new SSH key to run Ansible on the managed nodes:
```
ssh-keygen -t ed25519 -C "ansible@home"
```
