SHELL = /bin/bash

.PHONY: help bootstrap deps run
.DEFAULT_GOAL := help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | \
	awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

bootstrap: guard-TARGET guard-SSH_USER	## Bootstrap a new node with everything to be managed by Ansible
	@source venv/bin/activate && ansible-playbook bootstrap.yml -i inventories/production/ -l ${TARGET} --vault-pass-file vaultpass -e 'ansible_user=${SSH_USER}' --ask-pass -bK

deps:	## Install all dependencies
	@source venv/bin/activate && pip install -r requirements.txt && ansible-galaxy role install -r requirements.yml

run:	## Run the infra playbook
	@source venv/bin/activate && ansible-playbook infra.yml -i inventories/production/ --vault-pass-file vaultpass --private-key ssh_key/ansible_home_infra

guard-%:
	@#$(or ${$*}, $(error $* is not set))
